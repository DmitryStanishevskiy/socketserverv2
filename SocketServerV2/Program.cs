﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SocketServerV2
{
    class Program
    {
        static Dictionary<string, int> connectionInfo = new Dictionary<string, int>();
        static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                int port = Convert.ToInt32(args[0]);
                TcpListener server = null;
                {
                    IPAddress localAddr = IPAddress.Parse("127.0.0.1");

                    server = new TcpListener(localAddr, port);
                    server.Start();

                    while (true)
                    {
                        TcpClient tcpClient = server.AcceptTcpClient();
                        StartConnection(tcpClient);
                    }
                }
            }
        }

        static async void StartConnection(TcpClient tcpClient)
        {
            await Task.Run(() => {
                IPEndPoint endPoint = tcpClient.Client.RemoteEndPoint as IPEndPoint;
                string IP = $"{ endPoint.Address }:{ endPoint.Port }";
                connectionInfo.Add(IP, 0);
                NetworkStream stream = tcpClient.GetStream();
                SendMessage("Welcome\r\n", stream);
                string request = "";
                while (true)
                {
                    request = ReadMessage(stream);
                    if (request == null)
                        break;
                    string responce = RunHandlers(request, IP);
                    SendMessage(responce, stream);
                }
                connectionInfo.Remove(IP);
            });
        }

        static string ReadMessage(NetworkStream stream)
        {
            Byte[] bytes = new Byte[256];
            int i = 0;
            string message = "";
            while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
            {
                String data = Encoding.ASCII.GetString(bytes, 0, i);
                switch (data)
                {
                    case "\r\n":
                        return message;
                    case "\b":
                        message = message.Remove(message.Length - 1);
                        break;
                    default:
                        message += data;
                        break;
                }
            }
            return null;
        }

        static void SendMessage(string data, NetworkStream stream)
        {
            byte[] msg = Encoding.ASCII.GetBytes(data);
            stream.Write(msg, 0, msg.Length);
        }

        static string RunHandlers(string message, string IP)
        {
            if (message.ToLower().Trim().CompareTo("list") == 0)
            {
                string responce = "";
                foreach (var item in connectionInfo)
                {
                    responce += $"IP: { item.Key }, sum = { item.Value }\r\n";
                }
                return responce;
            }

            Regex regex = new Regex("^[0-9]+$");
            if (regex.IsMatch(message))
            {
                connectionInfo[IP] += Convert.ToInt32(message);
                return $"Total: { connectionInfo[IP] }\r\n";
            }

            if (message.Length == 0)
            {
                return "";
            }

            return $"Unknown command { message }\r\n";
        }
    }
}
